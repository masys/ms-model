"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function mapJsonApiRecord(response, record) {
  var obj = _objectSpread({}, record.attributes);

  for (var _i = 0, _Object$keys = Object.keys(record.attributes); _i < _Object$keys.length; _i++) {
    var key = _Object$keys[_i];
    Object.defineProperty(obj, key, {
      value: record.attributes[key],
      enumerable: true
    });
  }

  if (record.relationships && response.included) {
    var relationKeys = Object.keys(record.relationships);

    var _loop = function _loop() {
      var relationKey = _relationKeys[_i2];

      if (Array.isArray(record.relationships[relationKey].data)) {
        // Has Many Relations
        Object.defineProperty(obj, relationKey, {
          value: [],
          enumerable: true
        });

        var _iterator = _createForOfIteratorHelper(record.relationships[relationKey].data),
            _step;

        try {
          var _loop2 = function _loop2() {
            var relationObj = _step.value;
            var relation = response.included.find(function (incl) {
              return incl.type === relationObj.type && Number(incl.id) === Number(relationObj.id);
            });

            if (!relation) {
              return "continue";
            }

            obj[relationKey].push(mapJsonApiRecord(response, relation));
          };

          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var _ret = _loop2();

            if (_ret === "continue") continue;
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      } else if ((0, _typeof2["default"])(record.relationships[relationKey].data) === 'object') {
        var relation = response.included.find(function (incl) {
          var type = record.relationships[relationKey].data && record.relationships[relationKey].data.type;
          return incl.type === type && Number(incl.id) === Number(record.relationships[relationKey].data.id);
        });

        if (relation) {
          Object.defineProperty(obj, relationKey, {
            value: mapJsonApiRecord(response, relation),
            enumerable: true
          });
        }
      }
    };

    for (var _i2 = 0, _relationKeys = relationKeys; _i2 < _relationKeys.length; _i2++) {
      _loop();
    }
  }

  return obj;
}

module.exports = {
  mapJsonApiRecord: mapJsonApiRecord
};