"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var inflector = require('inflector-js');

var _require = require('./util'),
    mapJsonApiRecord = _require.mapJsonApiRecord;

var MSModel = /*#__PURE__*/function () {
  (0, _createClass2["default"])(MSModel, null, [{
    key: "all",
    value: function () {
      var _all = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(filter) {
        var url, nestedRoute, nestedColumn, resourceRoute, res;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                url = this.url;

                if (this.nestedResource) {
                  nestedRoute = inflector.pluralize(this.nestedResource);
                  nestedColumn = filter["".concat(this.nestedResource, "Id")] || filter["".concat(this.nestedResource, "_id")];
                  resourceRoute = this.url.replace(this.apiUrl, '');
                  url = "".concat(this.apiUrl, "/").concat(nestedRoute, "/").concat(nestedColumn).concat(resourceRoute);
                }

                if (this.stringifyParams) {
                  filter = JSON.stringify(filter);
                }

                _context.next = 5;
                return this._get(url, filter);

              case 5:
                res = _context.sent;
                return _context.abrupt("return", this._mapCollection(res));

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function all(_x) {
        return _all.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "findOne",
    value: function () {
      var _findOne = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(id) {
        var filter,
            url,
            nestedRoute,
            nestedColumn,
            resourceRoute,
            res,
            _args2 = arguments;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                filter = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : {};
                url = this.url;

                if (this.nestedResource) {
                  nestedRoute = inflector.pluralize(this.nestedResource);
                  nestedColumn = filter["".concat(this.nestedResource, "Id")] || filter["".concat(this.nestedResource, "_id")];
                  resourceRoute = this.url.replace(this.apiUrl, '');
                  url = "".concat(this.apiUrl, "/").concat(nestedRoute, "/").concat(nestedColumn).concat(resourceRoute);
                }

                if (this.stringifyParams) {
                  filter = JSON.stringify(filter);
                }

                _context2.next = 6;
                return this._get("".concat(url, "/").concat(id), filter);

              case 6:
                res = _context2.sent;

                if (!this.isJsonAPI) {
                  _context2.next = 9;
                  break;
                }

                return _context2.abrupt("return", new this(this.mapRecord(res, res.data)));

              case 9:
                if (!this.responseRootKey) {
                  _context2.next = 11;
                  break;
                }

                return _context2.abrupt("return", new this(res[this.responseRootKey]));

              case 11:
                return _context2.abrupt("return", new this(res));

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function findOne(_x2) {
        return _findOne.apply(this, arguments);
      }

      return findOne;
    }()
  }, {
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(payload) {
        var record;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                record = new this(payload);
                _context3.next = 3;
                return record.create();

              case 3:
                return _context3.abrupt("return", record);

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function create(_x3) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "count",
    value: function () {
      var _count = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(filter) {
        var url, nestedRoute, nestedColumn, resourceRoute, res;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                url = this.url;

                if (this.nestedResource) {
                  nestedRoute = inflector.pluralize(this.nestedResource);
                  nestedColumn = filter["".concat(this.nestedResource, "Id")] || filter["".concat(this.nestedResource, "_id")];
                  resourceRoute = this.url.replace(this.apiUrl, '');
                  url = "".concat(this.apiUrl, "/").concat(nestedRoute, "/").concat(nestedColumn).concat(resourceRoute);
                }

                if (this.stringifyParams) {
                  filter = JSON.stringify(filter);
                }

                _context4.next = 5;
                return this._get(url + '/count', filter);

              case 5:
                res = _context4.sent;

                if (!this.responseRootKey) {
                  _context4.next = 8;
                  break;
                }

                return _context4.abrupt("return", res[this.responseRootKey]);

              case 8:
                return _context4.abrupt("return", res);

              case 9:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function count(_x4) {
        return _count.apply(this, arguments);
      }

      return count;
    }()
  }, {
    key: "_get",
    value: function () {
      var _get2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(url, params) {
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (this.http) {
                  _context5.next = 2;
                  break;
                }

                throw new Error('MSModel HTTP Client has not been setup yet!');

              case 2:
                return _context5.abrupt("return", this.http.get(url, params));

              case 3:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function _get(_x5, _x6) {
        return _get2.apply(this, arguments);
      }

      return _get;
    }()
  }, {
    key: "_post",
    value: function () {
      var _post2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(url, params) {
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                if (this.http) {
                  _context6.next = 2;
                  break;
                }

                throw new Error('MSModel HTTP Client has not been setup yet!');

              case 2:
                return _context6.abrupt("return", this.http.post(url, params));

              case 3:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function _post(_x7, _x8) {
        return _post2.apply(this, arguments);
      }

      return _post;
    }()
  }, {
    key: "_patch",
    value: function () {
      var _patch2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(url, params) {
        return _regenerator["default"].wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                if (this.http) {
                  _context7.next = 2;
                  break;
                }

                throw new Error('MSModel HTTP Client has not been setup yet!');

              case 2:
                return _context7.abrupt("return", this.http.patch(url, params));

              case 3:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function _patch(_x9, _x10) {
        return _patch2.apply(this, arguments);
      }

      return _patch;
    }()
  }, {
    key: "_put",
    value: function () {
      var _put2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(url, params) {
        return _regenerator["default"].wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                if (this.http) {
                  _context8.next = 2;
                  break;
                }

                throw new Error('MSModel HTTP Client has not been setup yet!');

              case 2:
                return _context8.abrupt("return", this.http.put(url, params));

              case 3:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function _put(_x11, _x12) {
        return _put2.apply(this, arguments);
      }

      return _put;
    }()
  }, {
    key: "_delete",
    value: function () {
      var _delete2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(url) {
        return _regenerator["default"].wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                if (this.http) {
                  _context9.next = 2;
                  break;
                }

                throw new Error('MSModel HTTP Client has not been setup yet!');

              case 2:
                return _context9.abrupt("return", this.http["delete"](url));

              case 3:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      function _delete(_x13) {
        return _delete2.apply(this, arguments);
      }

      return _delete;
    }()
  }, {
    key: "_mapCollection",
    value: function _mapCollection(response) {
      var _this = this;

      var records = response;

      if (this.responseRootKey) {
        records = response[this.responseRootKey];
      }

      var collection = records.map(function (record) {
        if (_this.isJsonAPI) {
          return new _this(_this.mapRecord(response, record));
        }

        return new _this(record);
      });

      if (response.meta) {
        collection.meta = response.meta;
      }

      return collection;
    }
  }, {
    key: "mapRecord",
    value: function mapRecord(response, record) {
      return mapJsonApiRecord(response, record);
    } // Prototype -------------------------------------------------------------------------------------

  }, {
    key: "url",
    get: function get() {
      return this.apiUrl;
    }
  }, {
    key: "paramsWrapper",
    get: function get() {
      return null;
    }
  }, {
    key: "isJsonAPI",
    get: function get() {
      return this.adapter === 'json_api';
    }
  }, {
    key: "defaultAttributes",
    get: function get() {
      return {};
    }
  }, {
    key: "nestedResource",
    get: function get() {
      return null;
    }
  }]);

  function MSModel() {
    var attrs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    (0, _classCallCheck2["default"])(this, MSModel);

    this._setAttributes(Object.assign(this.constructor.defaultAttributes, attrs));
  }

  (0, _createClass2["default"])(MSModel, [{
    key: "_setAttributes",
    value: function _setAttributes(attrs) {
      var _this2 = this;

      if (!attrs) {
        return;
      }

      Object.defineProperty(this, 'oldValues', {
        value: attrs,
        enumerable: false,
        writable: false,
        configurable: true
      });
      Object.keys(attrs).forEach(function (key) {
        _this2._setAttribute(key, attrs[key]);
      });
    }
  }, {
    key: "_setAttribute",
    value: function _setAttribute(key, value) {
      this[key] = value;
      Object.defineProperty(this, key, {
        value: value,
        enumerable: true,
        writable: true
      });
    }
  }, {
    key: "save",
    value: function () {
      var _save = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10() {
        return _regenerator["default"].wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                if (!this.id) {
                  _context10.next = 4;
                  break;
                }

                return _context10.abrupt("return", this.update());

              case 4:
                return _context10.abrupt("return", this.create());

              case 5:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, this);
      }));

      function save() {
        return _save.apply(this, arguments);
      }

      return save;
    }()
  }, {
    key: "create",
    value: function () {
      var _create2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee11() {
        var payload, url, res;
        return _regenerator["default"].wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                payload = {};

                if (this.constructor.paramsWrapper) {
                  payload[this.constructor.paramsWrapper] = _objectSpread({}, this);
                } else {
                  payload = _objectSpread({}, this);
                }

                url = this.constructor.url;

                if (this.constructor.nestedResource) {
                  url = "".concat(this.constructor.apiUrl, "/").concat(this.nestedRoute, "/").concat(this.nestedColumn).concat(this.resourceRoute);
                }

                _context11.next = 6;
                return this.constructor._post(url, payload);

              case 6:
                res = _context11.sent;

                if (this.constructor.responseRootKey) {
                  this._setAttributes(res[this.constructor.responseRootKey]);
                } else {
                  this._setAttributes(res);
                }

                return _context11.abrupt("return", true);

              case 9:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11, this);
      }));

      function create() {
        return _create2.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "update",
    value: function () {
      var _update = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee12() {
        var payload, url, res;
        return _regenerator["default"].wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                payload = {};

                if (!this.constructor.paramsWrapper) {
                  _context12.next = 7;
                  break;
                }

                payload[this.constructor.paramsWrapper] = this.attributesToUpdate();

                if (Object.keys(payload[this.constructor.paramsWrapper]).length) {
                  _context12.next = 5;
                  break;
                }

                return _context12.abrupt("return", true);

              case 5:
                _context12.next = 10;
                break;

              case 7:
                payload = this.attributesToUpdate();

                if (Object.keys(payload).length) {
                  _context12.next = 10;
                  break;
                }

                return _context12.abrupt("return", true);

              case 10:
                url = this.constructor.url;

                if (this.constructor.nestedResource) {
                  url = "".concat(this.constructor.apiUrl, "/").concat(this.nestedRoute, "/").concat(this.nestedColumn).concat(this.resourceRoute);
                }

                if (!this.constructor.updateWithPut) {
                  _context12.next = 18;
                  break;
                }

                _context12.next = 15;
                return this.constructor._put("".concat(url, "/").concat(this.id), payload);

              case 15:
                res = _context12.sent;
                _context12.next = 21;
                break;

              case 18:
                _context12.next = 20;
                return this.constructor._patch("".concat(url, "/").concat(this.id), payload);

              case 20:
                res = _context12.sent;

              case 21:
                if (this.constructor.responseRootKey) {
                  this._setAttributes(res[this.constructor.responseRootKey]);
                } else {
                  this._setAttributes(res);
                }

                return _context12.abrupt("return", true);

              case 23:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12, this);
      }));

      function update() {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }, {
    key: "destroy",
    value: function () {
      var _destroy = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee13() {
        var url;
        return _regenerator["default"].wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                url = this.constructor.url;

                if (this.constructor.nestedResource) {
                  url = "".concat(this.constructor.apiUrl, "/").concat(this.nestedRoute, "/").concat(this.nestedColumn).concat(this.resourceRoute);
                }

                _context13.next = 4;
                return this.constructor._delete("".concat(url, "/").concat(this.id));

              case 4:
                return _context13.abrupt("return", true);

              case 5:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13, this);
      }));

      function destroy() {
        return _destroy.apply(this, arguments);
      }

      return destroy;
    }()
  }, {
    key: "attributesToUpdate",
    value: function attributesToUpdate() {
      if (this.constructor.sendAllAttributesOnUpdate) {
        return _objectSpread({}, this);
      }

      var attrs = {};
      var keys = Object.keys(this);

      for (var _i = 0, _keys = keys; _i < _keys.length; _i++) {
        var key = _keys[_i];

        if (this[key] !== this.oldValues[key]) {
          attrs[key] = this[key];
        }
      }

      return attrs;
    }
  }, {
    key: "nestedRoute",
    get: function get() {
      return inflector.pluralize(this.constructor.nestedResource);
    }
  }, {
    key: "nestedColumn",
    get: function get() {
      var value;

      if (this.oldValues) {
        value = this.oldValues["".concat(this.constructor.nestedResource, "Id")] || this.oldValues["".concat(this.constructor.nestedResource, "_id")];
      }

      if (value) {
        return value;
      }

      return this["".concat(this.constructor.nestedResource, "Id")] || this["".concat(this.constructor.nestedResource, "_id")];
    }
  }, {
    key: "resourceRoute",
    get: function get() {
      return this.constructor.url.replace(this.constructor.apiUrl, '');
    }
  }]);
  return MSModel;
}();

MSModel.apiUrl = null;
MSModel.http = null;
MSModel.responseRootKey = null;
MSModel.stringifyParams = false;
MSModel.sendAllAttributesOnUpdate = true;
MSModel.updateWithPut = false;
MSModel.adapter = 'json';
MSModel.mapJsonApiIncluded = false;
MSModel["default"] = MSModel;
module.exports = MSModel;