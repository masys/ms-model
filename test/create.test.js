const sinon = require('sinon')
const assert = require('assert')
const { MSHttp, MSModel } = require('./setup')

describe('MSModel', function() {
  describe('.create()', function () {
    const sandbox = sinon.createSandbox()

    beforeEach(() => {
      sandbox.spy(MSHttp)
    })

    afterEach(() => {
      sandbox.restore()
    })

    context('with params wrapper', () => {
      class User extends MSModel {
        static get url () {
          return this.apiUrl + '/users'
        }

        static get paramsWrapper () {
          return 'user'
        }
      }

      it('passes url to http client', async () => {
        await User.create({ name: 'John' })
        assert.equal(MSHttp.post.getCall(0).args[0], 'http://localhost:3000/api/users')
      })

      it('passes payload to http client', async () => {
        await User.create({ name: 'John' })
        assert.equal(MSHttp.post.getCall(0).args[1].user.name, 'John')
      })

      it('returns instance of resource', async () => {
        const user = await User.create({ name: 'John' })
        assert(user instanceof User)
      })
    })

    context('with no params wrapper', () => {
      class User extends MSModel {
        static get url () {
          return this.apiUrl + '/users'
        }
      }

      it('passes url to http client', async () => {
        await User.create({ name: 'John' })
        assert.equal(MSHttp.post.getCall(0).args[0], 'http://localhost:3000/api/users')
      })

      it('passes payload to http client', async () => {
        await User.create({ name: 'John' })
        assert.equal(MSHttp.post.getCall(0).args[1].name, 'John')
      })
    })
  })
})
