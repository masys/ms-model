const assert = require('assert')
const { User } = require('./setup')

describe('MSModel', function() {
  describe('.url', function () {
    it('sets url', () => {
      assert.equal(User.url, 'http://localhost:3000/api/users')
    })
  })
})
