const sinon = require('sinon')
const assert = require('assert')
const { MSHttp, MSModel } = require('./setup')

describe('MSModel', function() {
  describe('.create()', function () {
    const sandbox = sinon.createSandbox()

    beforeEach(() => {
      sandbox.spy(MSHttp)
    })

    afterEach(() => {
      sandbox.restore()
    })

    context('with nested resource', () => {
      class User extends MSModel {
        static get url () {
          return this.apiUrl + '/users'
        }

        static get paramsWrapper () {
          return 'user'
        }

        static get nestedResource () {
          return 'business_type'
        }
      }

      it('passes url to http client', async () => {
        await User.create({ name: 'John', business_type_id: 123 })
        assert.equal(
          MSHttp.post.getCall(0).args[0],
          'http://localhost:3000/api/business_types/123/users'
        )
      })
    })
  })
})
