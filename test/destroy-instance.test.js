const sinon = require('sinon')
const assert = require('assert')
const { MSHttp, User } = require('./setup')

describe('MSModel', function() {
  describe('#destroy()', function () {
    const sandbox = sinon.createSandbox()

    beforeEach(() => {
      sandbox.spy(MSHttp)
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('passes url to http client', async () => {
      const user = new User({ id: 1, name: 'John', lastname: 'Doe' })
      await user.destroy()
      assert.equal(MSHttp.delete.getCall(0).args[0], 'http://localhost:3000/api/users/1')
    })
  })
})
