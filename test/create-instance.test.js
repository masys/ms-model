const sinon = require('sinon')
const assert = require('assert')
const { MSHttp, MSModel } = require('./setup')

describe('MSModel', function() {
  describe('#create()', function () {
    const sandbox = sinon.createSandbox()

    beforeEach(() => {
      sandbox.spy(MSHttp)
    })

    afterEach(() => {
      sandbox.restore()
    })

    context('with params wrapper', () => {
      class User extends MSModel {
        static get url () {
          return this.apiUrl + '/users'
        }

        static get paramsWrapper () {
          return 'user'
        }
      }

      it('passes url to http client', async () => {
        const user = new User({ name: 'John', lastname: 'Doe' })
        await user.save()
        assert.equal(MSHttp.post.getCall(0).args[0], 'http://localhost:3000/api/users')
      })

      it('passes payload to http client', async () => {
        const user = new User({ name: 'John', lastname: 'Doe' })
        await user.save()
        assert.equal(MSHttp.post.getCall(0).args[1].user.name, 'John')
        assert.equal(MSHttp.post.getCall(0).args[1].user.lastname, 'Doe')
      })

      it('assigns id', async () => {
        const user = new User({ name: 'John', lastname: 'Doe' })
        await user.save()
        assert.equal(user.id, 1)
      })
    })

    context('with no params wrapper', () => {
      class User extends MSModel {
        static get url () {
          return this.apiUrl + '/users'
        }
      }

      it('passes url to http client', async () => {
        const user = new User({ name: 'John', lastname: 'Doe' })
        await user.save()
        assert.equal(MSHttp.post.getCall(0).args[0], 'http://localhost:3000/api/users')
      })

      it('passes payload to http client', async () => {
        const user = new User({ name: 'John', lastname: 'Doe' })
        await user.save()
        assert.equal(MSHttp.post.getCall(0).args[1].name, 'John')
        assert.equal(MSHttp.post.getCall(0).args[1].lastname, 'Doe')
      })

      it('assigns id', async () => {
        const user = new User({ name: 'John', lastname: 'Doe' })
        await user.save()
        assert.equal(user.id, 1)
      })
    })
  })
})
