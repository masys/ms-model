const sinon = require('sinon')
const assert = require('assert')
const { MSHttp, User } = require('./setup')

describe('MSModel', function() {
  describe('.count()', function () {
    const sandbox = sinon.createSandbox()

    beforeEach(() => {
      sandbox.spy(MSHttp)
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('returns count', async () => {
      const count = await User.count()
      assert.equal(count.count, 2)
    })

    it('passes url to http client', async () => {
      await User.count()
      assert.equal(MSHttp.get.getCall(0).args[0], 'http://localhost:3000/api/users/count')
    })

    it('passes params to http client', async () => {
      const filter = { where: 'test' }
      await User.count(filter)
      assert.equal(MSHttp.get.getCall(0).args[1].where, 'test')
    })
  })
})
