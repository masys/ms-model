const sinon = require('sinon')
const assert = require('assert')
const { MSHttp, MSModel } = require('./setup')

describe('MSModel', function() {
  describe('#update()', function () {
    const sandbox = sinon.createSandbox()

    beforeEach(() => {
      sandbox.spy(MSHttp)
    })

    afterEach(() => {
      sandbox.restore()
    })

    context('with params wrapper', () => {
      class User extends MSModel {
        static get url () {
          return this.apiUrl + '/users'
        }

        static get paramsWrapper () {
          return 'user'
        }

        static get nestedResource () {
          return 'business'
        }
      }

      it('passes url to http client', async () => {
        const user = new User({ id: 1, name: 'John', lastname: 'Doe', business_id: 123 })
        await user.save()
        assert.equal(
          MSHttp.patch.getCall(0).args[0],
          'http://localhost:3000/api/businesses/123/users/1'
        )
      })
    })
  })
})
