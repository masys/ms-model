const sinon = require('sinon')
const assert = require('assert')
const { MSModel, MSHttp } = require('./setup')
const jsonAPICollection = require('./fixtures/json-api-collection')

describe('MSModel', function() {
  describe('.all()', function () {
    context('normal json response', () => {
      const sandbox = sinon.createSandbox()
      class User extends MSModel {
        static get url () {
          return this.apiUrl + '/users'
        }

        static get defaultAttributes () {
          return {
            default_attributes_new: 'new default attribute value'
          }
        }

        static get nestedResource () {
          return 'business'
        }
      }

      beforeEach(() => {
        sandbox.spy(MSHttp)
      })

      afterEach(() => {
        sandbox.restore()
      })

      it('passes url to http client', async () => {
        await User.all({ business_id: 123 })
        assert.equal(
          MSHttp.get.getCall(0).args[0],
          'http://localhost:3000/api/businesses/123/users'
        )
      })
    })
  })
})
