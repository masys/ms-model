const sinon = require('sinon')
const assert = require('assert')
const { MSModel, MSHttp, User } = require('./setup')
const jsonAPICollection = require('./fixtures/json-api-collection')

describe('MSModel', function() {
  describe('.all()', function () {
    context('normal json response', () => {
      const sandbox = sinon.createSandbox()

      beforeEach(() => {
        sandbox.spy(MSHttp)
      })

      afterEach(() => {
        sandbox.restore()
      })

      it('returns array', async () => {
        const users = await User.all()
        assert.equal(users[0].id, 1)
      })

      it('returns array of resource', async () => {
        const users = await User.all()
        assert(users[0] instanceof User)
      })

      it('passes url to http client', async () => {
        await User.all()
        assert.equal(MSHttp.get.getCall(0).args[0], 'http://localhost:3000/api/users')
      })

      it('passes params to http client', async () => {
        const filter = { include: 'posts' }
        await User.all(filter)
        assert.equal(MSHttp.get.getCall(0).args[1].include, 'posts')
      })
    })

    context('when json api', () => {
      let stub

      beforeEach(async () => {
        MSModel.adapter = 'json_api'
        stub = sinon.stub(MSHttp, 'get')
        stub.returns(jsonAPICollection)
      })

      afterEach(() => {
        stub.restore()
        MSModel.adapter = 'json'
      })

      it('maps record attributes', async () => {
        const filter = { include: ['author', 'comments'] }
        const records = await User.all(filter)
        assert.equal(records[0].username, 'user_name')
      })

      it('maps record belong relation', async () => {
        const filter = { include: ['author', 'comments'] }
        const records = await User.all(filter)
        assert.equal(records[0].company.name, 'company name')
      })

      it('maps record has many relation', async () => {
        const filter = { include: ['author', 'comments'] }
        const records = await User.all(filter)
        assert.equal(records[0].comments.length, 2)
        assert.equal(records[0].comments[0].body, 'comment 1')
      })

      it('maps nested relations', async () => {
        const filter = { include: ['author', 'comments'] }
        const records = await User.all(filter)
        assert.equal(records[0].company.author.name, 'john')
      })
    })
  })
})
