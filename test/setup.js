const MSModel = require('../lib')

const collection = [
  { id: 1, name: 'John', lastname: 'Doe' },
  { id: 2, name: 'Name', lastname: 'LastName' }
]

const MSHttp = {
  async get (url, params) {
    const arr = url.split('/')
    const last = arr[arr.length - 1]

    if (isNaN(last)) {
      if (last === 'count') {
        return { data: { count: collection.length } }
      }
      return { data: collection }
    }

    return { data: collection.find(o => o.id === Number(last)) }
  },

  async post (url, params) {
    return { data: collection[0] }
  },

  async put (url, params) {
    const obj = { ...collection[0] }
    obj.name = 'new name'
    return { data: obj }
  },

  async patch (url, params) {
    const obj = { ...collection[0] }
    obj.name = 'new name'
    return { data: obj }
  },

  async delete (url) {
  }
}

MSModel.apiUrl = 'http://localhost:3000/api'
MSModel.http = MSHttp
MSModel.responseRootKey = 'data'
MSModel.stringifyParams = false
MSModel.sendAllAttributesOnUpdate = true
MSModel.updateWithPut = false

class User extends MSModel {
  static get url () {
    return this.apiUrl + '/users'
  }

  static get defaultAttributes () {
    return {
      default_attributes_new: 'new default attribute value'
    }
  }
}

module.exports = {
  MSModel,
  MSHttp,
  User
}
