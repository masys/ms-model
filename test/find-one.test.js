const sinon = require('sinon')
const assert = require('assert')
const { MSModel, MSHttp, User } = require('./setup')
const jsonAPIOne = require('./fixtures/json-api-one')

describe('MSModel', function () {
  describe('.findOne()', function () {
    context('normal json response', () => {
      const sandbox = sinon.createSandbox()

      beforeEach(() => {
        sandbox.spy(MSHttp)
      })

      afterEach(() => {
        sandbox.restore()
      })

      it('returns one user', async () => {
        const filter = { include: 'posts' }
        const user = await User.findOne(2, filter)
        assert.equal(user.id, 2)
      })

      it('returns instance of resource', async () => {
        const filter = { include: 'posts' }
        const user = await User.findOne(2, filter)
        assert(user instanceof User)
      })

      it('passes url to http client', async () => {
        await User.findOne(2)
        assert.equal(MSHttp.get.getCall(0).args[0], 'http://localhost:3000/api/users/2')
      })

      it('passes params to http client', async () => {
        const filter = { include: 'posts' }
        await User.findOne(2, filter)
        assert.equal(MSHttp.get.getCall(0).args[1].include, 'posts')
      })
    })

    context('when json api', () => {
      let stub

      beforeEach(async () => {
        MSModel.adapter = 'json_api'
        stub = sinon.stub(MSHttp, 'get')
        stub.returns(jsonAPIOne)
      })

      afterEach(() => {
        stub.restore()
        MSModel.adapter = 'json'
      })

      it('maps record attributes', async () => {
        const filter = { include: ['author', 'comments'] }
        const record = await User.findOne(1, filter)
        assert.equal(record.username, 'user_name')
      })

      it('maps record belong relation', async () => {
        const filter = { include: ['author', 'comments'] }
        const record = await User.findOne(1, filter)
        assert.equal(record.company.name, 'company name')
      })

      it('maps record has many relation', async () => {
        const filter = { include: ['author', 'comments'] }
        const record = await User.findOne(1, filter)
        assert.equal(record.comments.length, 2)
        assert.equal(record.comments[0].body, 'comment 1')
      })
    })
  })
})
