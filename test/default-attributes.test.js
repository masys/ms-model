const sinon = require('sinon')
const assert = require('assert')
const { MSHttp, User } = require('./setup')

describe('MSModel', function() {
  describe('.defaultAttributes', function () {
    const sandbox = sinon.createSandbox()
    let user

    beforeEach(() => {
      sandbox.spy(MSHttp)
      user = new User({ id: 1, name: 'John', lastname: 'Doe' })
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('merges and sets default attributes', async () => {
      assert.equal(user.default_attributes_new, 'new default attribute value')
    })

    it('merges and sets attributes from initialization', async () => {
      assert.equal(user.name, 'John')
    })
  })
})
