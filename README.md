# MS Model
UI Model Rest Client to communicate with API and map response into objects to better usage.

# Installation
```
$ npm i -S git+https://gitlab.com/masys/ms-model.git#0.1.1
```

# Usage

You will need a class/object to map rest client requests and assign it to MSModel lib:

```javascript
const MSHttp = {
  async get (url, params) {
    // make requests type GET
  },

  async post (url, params) {
    // make requests type POST
  },

  async put (url, params) {
    // make requests type PUT
  },

  async patch (url, params) {
    // make requests type PATCH
  },

  async delete (url) {
    // make requests type DELETE
  }
}
```
MSModel will call those functions to make requests to your API and you should retrun response to

MSModel to map it into corresponding resource.

assign and config MSModel:
```javascript
const MSModel = require('ms-model')

// API URL.
MSModel.apiUrl = 'YOUR_API_URL'
// HTTP class/object we declared before.
MSModel.http = MSHttp
// use this option if your response is wrapped.
MSModel.responseRootKey = 'customRootKey'
// if you need to send the filter as a string
MSModel.stringifyParams = false
// Send all attributes on update or send only the modified one.
MSModel.sendAllAttributesOnUpdate = true
// Update resource with PUT method.
MSModel.updateWithPut = false
// Enable JSON API.
MSModel.adapter = 'json_api'
// If true, and JSON API enabled will map included relations to attributes.
MSModel.mapJsonApiIncluded = false
```

now you can declare your resource class:
```javascript
const MSModel = require('ms-model')

class User extends MSModel {
  static get url () {
    return this.apiUrl + '/users'
  }

  // Wrap you params for create and update, like Ruby on Rails
  static get paramsWrapper () {
    return 'user'
  }

  // Set default attributes for the model.
  static get defaultAttributes () {
    return {
      active: true
    }
  }

  // Be sure to decalre the resource as singular and not plural
  // also you need to assign the attribute to the instance as `businessId` or `business_id`
  static get nestedResource () {
    return 'business'
  }
}
```

lets say that your API is a Rails app and you work with CRUD User resource:
```javascript
// returns all users, this will call:
// GET API_URL/users
users = await User.all()

// if you want to specify a filter, dependig on your API
users = await User.all({ where: { id: [1, 2, 3] } })

// find one resource by ID, filter is optional:
// GET API_URL/users/:id
user = await User.findOne(1, filter)

// create user directly:
// POST API_URL/users
user = await User.create(payload)

// count users by criteria:
// GET API_URL/users/count
count = await User.count(filter)

// initialize user:
user = new User({ name: 'John' })

// save user, this will check if the resource instance has id then will call update function else will create a new one:
await user.save()

// also you can call `#create()` directly:
await user.create()

// or update it `#update()`:
await user.update()
```

# License

[MIT](http://opensource.org/licenses/MIT)
