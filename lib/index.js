const inflector = require('inflector-js')
const { mapJsonApiRecord } = require('./util')

class MSModel {
  static get url () {
    return this.apiUrl
  }

  static get paramsWrapper () {
    return null
  }

  static get isJsonAPI () {
    return this.adapter === 'json_api'
  }

  static get defaultAttributes () {
    return {}
  }

  static get nestedResource () {
    return null
  }

  static async all (filter) {
    let url = this.url
    if (this.nestedResource) {
      const nestedRoute = inflector.pluralize(this.nestedResource)
      const nestedColumn = filter[`${this.nestedResource}Id`] || filter[`${this.nestedResource}_id`]
      const resourceRoute = this.url.replace(this.apiUrl, '')
      url = `${this.apiUrl}/${nestedRoute}/${nestedColumn}${resourceRoute}`
    }

    if (this.stringifyParams) {
      filter = JSON.stringify(filter)
    }

    const res = await this._get(url, filter)
    return this._mapCollection(res)
  }

  static async findOne (id, filter = {}) {
    let url = this.url
    if (this.nestedResource) {
      const nestedRoute = inflector.pluralize(this.nestedResource)
      const nestedColumn = filter[`${this.nestedResource}Id`] || filter[`${this.nestedResource}_id`]
      const resourceRoute = this.url.replace(this.apiUrl, '')
      url = `${this.apiUrl}/${nestedRoute}/${nestedColumn}${resourceRoute}`
    }

    if (this.stringifyParams) {
      filter = JSON.stringify(filter)
    }
    const res = await this._get(`${url}/${id}`, filter)

    if (this.isJsonAPI) {
      return new this(this.mapRecord(res, res.data))
    }

    if (this.responseRootKey) {
      return new this(res[this.responseRootKey])
    }

    return new this(res)
  }

  static async create (payload) {
    const record =  new this(payload)
    await record.create()
    return record
  }

  static async count (filter) {
    let url = this.url
    if (this.nestedResource) {
      const nestedRoute = inflector.pluralize(this.nestedResource)
      const nestedColumn = filter[`${this.nestedResource}Id`] || filter[`${this.nestedResource}_id`]
      const resourceRoute = this.url.replace(this.apiUrl, '')
      url = `${this.apiUrl}/${nestedRoute}/${nestedColumn}${resourceRoute}`
    }

    if (this.stringifyParams) {
      filter = JSON.stringify(filter)
    }
    const res = await this._get(url + '/count', filter)

    if (this.responseRootKey) {
      return res[this.responseRootKey]
    }
    return res
  }

  static async _get (url, params) {
    if (!this.http) { throw new Error('MSModel HTTP Client has not been setup yet!') }

    return this.http.get(url, params)
  }

  static async _post (url, params) {
    if (!this.http) { throw new Error('MSModel HTTP Client has not been setup yet!') }

    return this.http.post(url, params)
  }

  static async _patch (url, params) {
    if (!this.http) { throw new Error('MSModel HTTP Client has not been setup yet!') }

    return this.http.patch(url, params)
  }

  static async _put (url, params) {
    if (!this.http) { throw new Error('MSModel HTTP Client has not been setup yet!') }

    return this.http.put(url, params)
  }

  static async _delete (url) {
    if (!this.http) { throw new Error('MSModel HTTP Client has not been setup yet!') }

    return this.http.delete(url)
  }

  static _mapCollection (response) {
    let records = response
    if (this.responseRootKey) {
      records = response[this.responseRootKey]
    }

    const collection = records.map(record => {
      if (this.isJsonAPI) {
        return new this(this.mapRecord(response, record))
      }

      return new this(record)
    })

    if (response.meta) {
      collection.meta = response.meta
    }
    return collection
  }

  static mapRecord (response, record) {
    return mapJsonApiRecord(response, record)
  }

  // Prototype -------------------------------------------------------------------------------------

  constructor (attrs = {}) {
    this._setAttributes(Object.assign(this.constructor.defaultAttributes, attrs))
  }

  _setAttributes (attrs) {
    if (!attrs) { return }

    Object.defineProperty(this, 'oldValues', {
      value: attrs,
      enumerable: false,
      writable: false,
      configurable: true
    })

    Object.keys(attrs).forEach(key => {
      this._setAttribute(key, attrs[key])
    })
  }

  _setAttribute (key, value) {
    this[key] = value
    Object.defineProperty(this, key, {
      value: value,
      enumerable: true,
      writable: true
    })
  }

  async save () {
    if (this.id) {
      return this.update()
    } else {
      return this.create()
    }
  }

  get nestedRoute () {
    return inflector.pluralize(this.constructor.nestedResource)
  }

  get nestedColumn () {
    let value
    if (this.oldValues) {
      value = this.oldValues[`${this.constructor.nestedResource}Id`] ||
        this.oldValues[`${this.constructor.nestedResource}_id`]
    }
    if (value) { return value }

    return this[`${this.constructor.nestedResource}Id`] ||
      this[`${this.constructor.nestedResource}_id`]
  }

  get resourceRoute () {
    return this.constructor.url.replace(this.constructor.apiUrl, '')
  }

  async create () {
    let payload = {}
    if (this.constructor.paramsWrapper) {
      payload[this.constructor.paramsWrapper] = { ...this }
    } else {
      payload = { ...this }
    }
    let url = this.constructor.url
    if (this.constructor.nestedResource) {
      url = `${this.constructor.apiUrl}/${this.nestedRoute}/${this.nestedColumn}${this.resourceRoute}`
    }
    const res = await this.constructor._post(url, payload)

    if (this.constructor.responseRootKey) {
      this._setAttributes(res[this.constructor.responseRootKey])
    } else {
      this._setAttributes(res)
    }

    return true
  }

  async update () {
    let payload = {}
    if (this.constructor.paramsWrapper) {
      payload[this.constructor.paramsWrapper] = this.attributesToUpdate()
      if (!Object.keys(payload[this.constructor.paramsWrapper]).length) { return true }
    } else {
      payload = this.attributesToUpdate()
      if (!Object.keys(payload).length) { return true }
    }

    let url = this.constructor.url
    if (this.constructor.nestedResource) {
      url = `${this.constructor.apiUrl}/${this.nestedRoute}/${this.nestedColumn}${this.resourceRoute}`
    }
    let res
    if (this.constructor.updateWithPut) {
      res = await this.constructor._put(`${url}/${this.id}`, payload)
    } else {
      res = await this.constructor._patch(`${url}/${this.id}`, payload)
    }
    if (this.constructor.responseRootKey) {
      this._setAttributes(res[this.constructor.responseRootKey])
    } else {
      this._setAttributes(res)
    }

    return true
  }

  async destroy () {
    let url = this.constructor.url
    if (this.constructor.nestedResource) {
      url = `${this.constructor.apiUrl}/${this.nestedRoute}/${this.nestedColumn}${this.resourceRoute}`
    }
    await this.constructor._delete(`${url}/${this.id}`)

    return true
  }

  attributesToUpdate () {
    if (this.constructor.sendAllAttributesOnUpdate) {
      return { ...this }
    }

    const attrs = {}
    const keys = Object.keys(this)
    for (let key of keys) {
      if (this[key] !== this.oldValues[key]) {
        attrs[key] = this[key]
      }
    }
    return attrs
  }
}

MSModel.apiUrl = null
MSModel.http = null
MSModel.responseRootKey = null
MSModel.stringifyParams = false
MSModel.sendAllAttributesOnUpdate = true
MSModel.updateWithPut = false
MSModel.adapter = 'json'
MSModel.mapJsonApiIncluded = false
MSModel.default = MSModel

module.exports = MSModel
