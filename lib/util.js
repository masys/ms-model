function mapJsonApiRecord (response, record) {
  const obj = { ...record.attributes }
  for (let key of Object.keys(record.attributes)) {
    Object.defineProperty(obj, key, { value: record.attributes[key], enumerable: true })
  }

  if (record.relationships && response.included) {
    const relationKeys = Object.keys(record.relationships)
    for (let relationKey of relationKeys) {
      if (Array.isArray(record.relationships[relationKey].data)) {

        // Has Many Relations
        Object.defineProperty(obj, relationKey, { value: [], enumerable: true })
        for (let relationObj of record.relationships[relationKey].data) {
          const relation = response.included.find(incl => {
            return incl.type === relationObj.type &&
              Number(incl.id) === Number(relationObj.id)
          })
          if (!relation) { continue }

          obj[relationKey].push(mapJsonApiRecord(response, relation))
        }

      } else if (typeof record.relationships[relationKey].data === 'object') {
        const relation = response.included.find(incl => {
          const type = record.relationships[relationKey].data && record.relationships[relationKey].data.type
          return incl.type === type &&
            Number(incl.id) === Number(record.relationships[relationKey].data.id)
        })
        if (relation) {
          Object.defineProperty(obj, relationKey, { value: mapJsonApiRecord(response, relation), enumerable: true })
        }
      }
    }
  }

  return obj
}

module.exports = {
  mapJsonApiRecord
}
